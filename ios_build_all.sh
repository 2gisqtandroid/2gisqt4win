rm -rf ../qt-device
rm -rf ../qt-simulator
./ios.rb --shadow=qt-device --platform=device --loglevel=all --build=j1
./ios.rb --shadow=qt-simulator --platform=simulator --loglevel=all --build=j1
cd ..
rm -rf QtForiOS
mkdir -p QtForiOS
cd qt-device/
make install
cd ..
cp -R /usr/local/Trolltech/Qt-4.7.4/* QtForiOS/
cd QtForiOS/lib/
rm libQtCore.a libQtNetwork.a
cd ../../
lipo -create qt-device/lib/libQtCore.a qt-simulator/lib/libQtCore.a -output QtForiOS/lib/libQtCore.a
lipo -create qt-device/lib/libQtNetwork.a qt-simulator/lib/libQtNetwork.a -output QtForiOS/lib/libQtNetwork.a
rm -rf /usr/local/Trolltech/
