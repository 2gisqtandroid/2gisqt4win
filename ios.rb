#!/usr/bin/env ruby

config = []

config << '-commercial' << '-confirm-license' << '-no-pch' << '-release' << '-no-phonon' << '-no-freetype'
config << '-fast' << '-little-endian' << '-no-qt3support' << '-no-largefile'
config << '-nomake demos' << '-nomake examples' << '-reduce-exports' << '-no-exceptions' << '-no-stl' << '-no-xmlpatterns'
config << '-no-multimedia' << '-no-phonon-backend' << '-no-webkit' << '-no-script' << '-no-javascript-jit'
config << '-no-scripttools' << '-no-openssl' << '-no-dbus' << '-no-accessibility' << '-no-declarative' << '-no-gtkstyle'
config << '-no-opengl' << '-no-glib' << '-no-libmng' << '-no-libtiff' << '-no-libjpeg' << '-no-gif'
config << '-no-svg' << '-no-sql-sqlite' << '-nomake tests' << '-no-accessibility' << '-no-sql-mysql'
config << '-no-sql-odbc' << '-no-cups' << '-nomake tools'
config << '-static' << '-no-iconv' << '-nomake docs' << '-nomake translations'
config << '-host-little-endian' 
config << '-no-gui'

def help
  puts "Qt for iOS configuration script"
  puts "Use this script to build a static Qt libraries"
  puts "--shadow=shadowdir - required. Shadow build make your life easier."
  puts "--build=j{1..6} after successfull configuration"
  puts "--platform=simulator|device for configure Qt for build lib for device or simulator"
  puts "--loglevel=all|err all logs or errors and warnings only. Second default"
  exit 0
end

def log_func(log_level)
  case log_level
  when "all"
    puts "Log level: all"
    lambda {|str| system(str)}
  else
    puts "Log level: errors and warnings only"
    lambda {|str| `#{str}`}
  end
end

def shadow(shadow_dir, config, log_level)
  basename = File.basename `pwd`
  log = log_func(log_level)
  log.call("mkdir -p ../#{shadow_dir}")
  log.call("cd ../#{shadow_dir} && ../#{basename.chomp}/configure #{config.join(' ')}")
end

def build(build_dir, make_ops, log_level)
  log = log_func(log_level)
  log.call("cd ../#{build_dir} && make -#{make_ops}")
end

#############################################
# Processing command line arguments to hash #
#############################################

opts = {}
ARGV.each do |value|
  str = value.gsub('--', '')
  if str.include? '='
    temp = str.split('=')
    opts[temp[0].to_sym] = temp[1]
  else
    opts[str.to_sym] = str
  end
end

help if opts[:help] or opts.empty?
help if not opts[:shadow]

case opts[:platform]
when "simulator"
  config << '-platform macx-ios-g++42 -xplatform ios-simulator-llvm'
when "device"
  config << '-platform macx-ios-g++42 -xplatform ios-device-arm-llvm'
else
  puts "Error: --platform required"
  help
end

shadow(opts[:shadow], config, opts[:loglevel]) if opts[:shadow]
build(opts[:shadow], opts[:build], opts[:loglevel]) if opts[:build]
